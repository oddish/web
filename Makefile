CC=gcc
CFLAGS=-I.
WEB_ROOT=/home/oddish/public_html

copy: build
	cp cgi-bin/* $(WEB_ROOT)/cgi-bin/
	cp public_html/* $(WEB_ROOT)/

build: main.c
	$(CC) -o cgi-bin/main.cgi main.c $(CFLAGS)

clean:
	rm cgi-bin/*.cgi
