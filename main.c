#include <stdio.h>

#define HTTP_HEADERS "content-type: text/html\n\n"
#define HEAD_HTML "<h1>$ ~oddish</h1>"\
                  "<hr>"
#define FOOT_HTML "<footer><a href='../'>../</a></footer>"
#define HOME_CONTENT "<p>Programmer by day, programmer by night. Values simplicity, being ecological and kindness. Also likes making music and cooking.</p>"

int main(void) {
    // Headers
    printf(HTTP_HEADERS);

    // HTML
    printf(HEAD_HTML);
    printf(HOME_CONTENT);
    printf(FOOT_HTML);
    return 0;
}
